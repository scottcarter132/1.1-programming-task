package ca1.task3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class ArrayCountTest {


	@Test
	void testcountOne() {
		ArrayCount C = new ArrayCount();
		
		String[] input = {"Not found","Not here","None","Found"};
		int expected = 1;
		
		assertEquals(expected, C.count(input,"Found"), "The count method does not work as expected, should return 1.");
	}
	
	@Test
	void testcountZero() {
		ArrayCount C = new ArrayCount();
		
		String[] input = {"Not found","Not here","None"};
		int expected = 0;
		
		assertEquals(expected, C.count(input,"Found"), "The count method does not work as expected, should return 0.");
	}
	
	@Test
	void testcountMultiple() {
		ArrayCount C = new ArrayCount();
		
		String[] input = {"Found","Not found","Not here","Found","Found"};
		int expected = 3;
		
		assertEquals(expected, C.count(input,"Found"), "The count method does not work as expected, should return 3.");
	}

}
