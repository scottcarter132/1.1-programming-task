package ca1.task3;
 //method uses loop to add 1 to string count for every time the array string is equal to the target string
public class ArrayCount {
	public int count(String[] array, String target) {
		int stringCount = 0;
		for (int i = 0; i < array.length; i++) {
			  if (target.equals(array[i])) {
				  stringCount ++;
			  }
			}
		return stringCount;
	}
}
