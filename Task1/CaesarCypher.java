package ca1.task1;

public class CaesarCypher {
	//method takes char array and input, runs each index of array through loop and checks if its is within the abc range
	//to covnvert to Ascii Numerical, adds the offset to the number and checks if its in range of the
	//lowercase abc, subtracts the appropriate amount to ensure looping back through the alphabet if not.
	//then converts the numerical back to the char equivalent,sets it within the array and returns the completed array
	public char[] encode(char[] input, int offset) {
		for (int i = 0; i < input.length; i++) {
			if ((int)(input[i]) < 97 || (int)(input[i]) > 122) {
				System.out.println("Input not allowed, must be lowercase alphabet.");
				return null;
			}
			else {
				int newInt = (int)(input[i]) + offset;
				if (newInt > 122) {
					newInt = newInt - 26;
				}
				input[i] = (char)(newInt);
			}
		}
		return input;
	}
	//method takes char array and input, runs each index of array through loop and checks if its is within the abc range
	//to covnvert to Ascii Numerical, subtracts the offset of the number and checks if its in range of the
	//lowercase abc, adds the appropriate amount to ensure looping back through the alphabet if not.
	//then converts the numerical back to the char equivalent,sets it within the array and returns the completed array
	public char[] decode(char[] input, int offset) {
		for (int i = 0; i < input.length; i++) {
			if ((int)(input[i]) < 97 || (int)(input[i]) > 122) {
				System.out.println("Input not allowed, must be lowercase alphabet.");
				return null;
			}
			else {
				int newInt = (int)(input[i]) - offset;
				if (newInt < 97) {
					newInt = newInt + 26;
				}
				input[i] = (char)(newInt);
			}
		}
		return input;
		}
}
