package ca1.task1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CaesarCypherTest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testEncode() {
		CaesarCypher cc = new CaesarCypher();
		
		char[] input = {'a', 'b', 'c'};
		char[] expected = {'c', 'd', 'e'};
		
		assertArrayEquals(expected, cc.encode(input, 2), "The encoding method does not work as expected.");
	}

	@Test
	void testDecode() {
		CaesarCypher cc = new CaesarCypher();
		
	
		char[] input = {'c', 'd', 'e'};
		char[] expected = {'a', 'b', 'c'};
		
		assertArrayEquals(expected, cc.decode(input, 2), "The decoding method does not work as expected.");
	}
	
	@Test
	void testEncodeReloopABC() {
		CaesarCypher cc = new CaesarCypher();
		
		char[] input = {'x', 'y', 'z'};
		char[] expected = {'z', 'a', 'b'};
		
		assertArrayEquals(expected, cc.encode(input, 2), "The encoding method does not work as expected, does not reloop lowercase alphabet.");
	}

	@Test
	void testDecodeReloopABC() {
		CaesarCypher cc = new CaesarCypher();
		
	
		char[] input = {'z', 'a', 'b'};
		char[] expected = {'x', 'y', 'z'};
		
		assertArrayEquals(expected, cc.decode(input, 2), "The decoding method does not work as expected, does not reloop lowercase alphabet.");
	}
	@Test
	void testEncodeWrongInput() {
		CaesarCypher cc = new CaesarCypher();
		
		char[] input = {' ', '[', '4'};
		
		assertArrayEquals(null, cc.encode(input, 2), "The encoding method does not work as expected, accepts wrong input.");
	}

	@Test
	void testDecodeWrongInput() {
		CaesarCypher cc = new CaesarCypher();
		
	
		char[] input = {' ', '[', '4'};
		
		assertArrayEquals(null, cc.decode(input, 2), "The decoding method does not work as expected, accepts wrong input.");
	}
	
}
