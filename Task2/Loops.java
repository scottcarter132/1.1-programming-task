package ca1.task2;

public class Loops {
	// method takes integer and runs loop to print "words" X the Integer 
	public void printWords(int numberOfTimes) {
		for (int i = 0; i < numberOfTimes; i++) {
			  System.out.println("words");
			}
	}
	// main method creates instance of Loops object and calls the printWords method.
	public static void main(String[] args) {
		Loops L = new Loops();
		L.printWords(10);
	}
}
