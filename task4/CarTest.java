package ca1.task4;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ca1.task3.ArrayCount;

public class CarTest {
	@Test
	void toStringTest() {
		Car C = new Car("Honda", "Civic", "Red");
		
		String expected = "Honda, Civic, Red";
		
		assertEquals(expected, C.toString(), "The toString method does not work as expected, should return a string in the form of “<Make>, <Model>, <Color>”.");
	}
	
	@Test
	void equalsTestTrue() {
		Car C = new Car("Honda", "Civic", "Red");
		Car CA = new Car("Honda", "Civic", "Red");
		
		boolean expected = true;
		
		assertEquals(expected, C.equals(CA), "The equals method does not work as expected, should return true.");
	}
	@Test
	void equalsTestFalse() {
		Car C = new Car("Honda", "Civic", "Red");
		Car CA = new Car("Honda", "Civic", "Blue");
		
		boolean expected = false;
		
		assertEquals(expected, C.equals(CA), "The equals method does not work as expected, should return false.");
	}
}




