package ca1.task4;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ca1.task3.ArrayCount;

public class CarProcessingTest {
	@Test
	void removeDuplicatesSingle() {
	CarProcessing cp = new CarProcessing();
			
			Car[] input = { 
					new Car("Honda", "Civic", "Red"),
					new Car("Holden", "Civic", "Red"),
					new Car("Jeep", "Wrangler", "Blue"),
					new Car("Jeep", "Wrangler", "Green"),
					new Car("Holden", "Falcon", "Yellow"),
					new Car("Honda", "Civic", "Blue"),
					new Car("Honda", "Falcon", "Blue"),
					new Car("Honda", "Civic", "Purple")
			};
		Car[] expected = {
				new Car("Honda", "Civic", "Red"),
				new Car("Jeep", "Wrangler", "Blue"),
				new Car("Jeep", "Wrangler", "Green"),
				new Car("Holden", "Falcon", "Yellow"),
				new Car("Honda", "Civic", "Blue"),
				new Car("Honda", "Falcon", "Blue"),
				new Car("Honda", "Civic", "Purple")
		};
		
		assertArrayEquals(expected, cp.removeDuplicates(input), "The removeDuplicate method does not work as expected, should remove one element from array");
	}
	@Test
	void removeDuplicatesMultiple() {
	CarProcessing cp = new CarProcessing();
			
			Car[] input = { 
					new Car("Honda", "Civic", "Red"),
					new Car("Holden", "Civic", "Red"),
					new Car("Jeep", "Wrangler", "Blue"),
					new Car("Jeep", "Wrangler", "Green"),
					new Car("Jeep", "Wrangler", "Green"),
					new Car("Holden", "Falcon", "Yellow"),
					new Car("Honda", "Civic", "Blue"),
					new Car("Honda", "Civic", "Blue"),
					new Car("Honda", "Civic", "Purple")
			};
		Car[] expected = {
				new Car("Honda", "Civic", "Red"),
				new Car("Jeep", "Wrangler", "Blue"),
				new Car("Jeep", "Wrangler", "Green"),
				new Car("Holden", "Falcon", "Yellow"),
				new Car("Honda", "Civic", "Blue"),
				new Car("Honda", "Civic", "Purple")
		};
		
		assertArrayEquals(expected, cp.removeDuplicates(input), "The removeDuplicate method does not work as expected, should remove 3 elements from array");
	}
	@Test
	void removeDuplicatesNone() {
	CarProcessing cp = new CarProcessing();
			
			Car[] input = { 
					new Car("Honda", "Civic", "Red"),
					new Car("Jeep", "Wrangler", "Blue"),
					new Car("Jeep", "Wrangler", "Green"),
					new Car("Holden", "Falcon", "Yellow"),
					new Car("Honda", "Civic", "Blue"),
					new Car("Honda", "Falcon", "Blue"),
					new Car("Honda", "Civic", "Purple")
			};
		Car[] expected = {
				new Car("Honda", "Civic", "Red"),
				new Car("Jeep", "Wrangler", "Blue"),
				new Car("Jeep", "Wrangler", "Green"),
				new Car("Holden", "Falcon", "Yellow"),
				new Car("Honda", "Civic", "Blue"),
				new Car("Honda", "Falcon", "Blue"),
				new Car("Honda", "Civic", "Purple")
		};
		
		assertArrayEquals(expected, cp.removeDuplicates(input), "The removeDuplicate method does not work as expected, should remove no elements from array");
	}
}

