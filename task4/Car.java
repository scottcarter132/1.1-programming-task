package ca1.task4;

public class Car {
	private String make;
	private String model;
	private String color;
	
	//constructor for Car object
	public Car(String make, String model, String color) {
		this.make = make;
		this.model = model;
		this.color = color;
	}
	
	//method to return string including make model etc as string from object
	@Override
	public String toString() {
		return this.make+", "+this.model+", "+this.color;	
	}
	
	//method that tests if object passed and cast as car has the same model and color as the object car.
	@Override
	public boolean equals(Object x) {
		 Car carObject = (Car) x;
		if  (carObject.color == this.color && carObject.model == this.model) {
			return true;
		}
		else {
			return false;
		}
	}
	
	//Overrides objects hashcode method.
	@Override
	public int hashCode() {
		return 0;
		
	}
	
}
