package ca1.task4;

public class CarProcessing {
	public static void main(String[] args) {
		
		CarProcessing cp = new CarProcessing();
		
		Car[] cars = { 
				new Car("Honda", "Civic", "Red"),
				new Car("Holden", "Civic", "Red"),
				new Car("Jeep", "Wrangler", "Blue"),
				new Car("Jeep", "Wrangler", "Green"),
				new Car("Holden", "Falcon", "Yellow"),
				new Car("Honda", "Civic", "Blue"),
				new Car("Honda", "Falcon", "Blue"),
				new Car("Honda", "Civic", "Purple")
		};
		
		Car[] noDuplicates = cp.removeDuplicates(cars);
		
		for(Car c : noDuplicates ) {
			System.out.println(c);
		}
	}
	
	//runs array through loops to test each array element with every other element in array, if they have the same color and model this will run a loop that creates a new array and fills it with every
	//other element other then the matching one,then saves the new array over the original thus essentially removing any other element that matches the first one tested.
	public Car[] removeDuplicates(Car[] inputArray) {
		for (int i = 0; i < inputArray.length; i++) {
			for (int n = i+1; n < inputArray.length; n++) {
				if (inputArray[i].equals(inputArray[n])) {
					Car[] newArray =  new Car[inputArray.length-1];
					for (int y =0, j = 0; y < inputArray.length; y++) {
						if (y != n) {
							newArray[j] = inputArray[y];
							j++;
							}
						}
							inputArray = newArray;
				}
			}
		}
		return inputArray;
	}
}
